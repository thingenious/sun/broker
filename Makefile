.DEFAULT_GOAL := help

.PHONY: help
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Default target: help"
	@echo ""
	@echo "Targets:"
	@echo " help            Show this message and exit"
	@echo " format          Format the code"
	@echo " lint            Lint the code"
	@echo " forlint         Alias for 'make format && make lint'"
	@echo " start           Start the mqtt container"
	@echo " stop            Stop the mqtt container"
	@echo " restart         Alias for 'make stop && make start'"
	@echo " users           Generate users.csv to import at the dashboard page"
	@echo ""

.PHONY: format
format:
	isort .
	autoflake --remove-all-unused-imports --remove-unused-variables --in-place .
	black --config pyproject.toml .
	ruff format --config pyproject.toml .

.PHONY: lint
lint:
	isort --check-only .
	black --check --config pyproject.toml .
	mypy --config pyproject.toml .
	flake8 --config=.flake8
	pydocstyle --config pyproject.toml .
	bandit -r -c pyproject.toml .
	yamllint -c .yamllint.yaml .
	ruff check --config pyproject.toml .
	pylint --rcfile=pyproject.toml --recursive y --output-format=text **/*.py

.PHONY: forlint
forlint: format lint

.PHONY: start
start:
	python -m pip install python-dotenv
	python scripts/start.py

.PHONY: stop
stop:
	python scripts/stop.py

.PHONY: restart
restart: stop start

.PHONY: users
users:
	python scripts/users.py
