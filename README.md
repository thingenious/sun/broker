# MQTT broker

Containerized [EMQX](https://www.emqx.io/) MQTT broker.

## Requirements

[Docker](https://docs.docker.com/get-docker/) or [Podman](https://podman.io/docs/installation)

### Optional Requirements

- [Python >= 3.8](https://www.python.org/downloads/)
- [Make](https://www.gnu.org/software/make/)

## Environment variables

Use `cp .env.template .env` and override if needed.

```text
MQTT_AUTHENTICATION_ENABLED=true
MQTT_USERS=comma,separated,users
MQTT_PASSWORDS=passwords,comma,separated
# the MQTT_ERLANG_COOKIE
# can be left empty (if not using a cluster)
# https://www.emqx.io/docs/en/latest/deploy/cluster/security.html
# if empty, a random value will be generated in scripts/start.py
# example random generation with:
# python -c "import secrets; print(secrets.token_hex(32))"
MQTT_ERLANG_COOKIE=
```

On windows, you can use:

```bash
python scripts/start.py
# or (if `make` is installed)
make start
```

Start with authentication disabled:

```bash
# change `podman` to `docker` if you are using docker
podman run --rm --name broker -p 18083:18083 -p 1883:1883 --init -d \
-e EMQX_TELEMETRY__ENABLE=false \
emqx:latest
```

Start with authentication enabled and with resource limits:

```bash
# node {
#   max_ports = 1034 # 1024 + number of max expected connections
#   process_limit = 1034
# change `podman` to `docker` if you are using docker
podman run --rm --name broker -p 18083:18083 -p 1883:1883 --init -d \
-e EMQX_TELEMETRY__ENABLE=false \
-e EMQX_AUTHENTICATION__1__ENABLE=true \
-e EMQX_AUTHENTICATION__1__BACKEND=built_in_database \
-e EMQX_AUTHENTICATION__1__MECHANISM=password_based \
-e EMQX_AUTHENTICATION__1__PASSWORD_HASH_ALGORITHM__NAME=sha256 \
-e EMQX_AUTHENTICATION__1__PASSWORD_HASH_ALGORITHM__SALT_POSITION=prefix \
-e EMQX_AUTHENTICATION__1__USER_ID_TYPE=username \
-e EMQX_NODE_MAX_PORTS=1034 \
-e EMQX_NODE_PROCESS_LIMIT=1034 \
emqx:latest
```

Initial dashboard (localhost:18083) login: `admin` / `public`

Include volumes to persist data:

```bash
# change `podman` to `docker` if you are using docker
# on linux: add ` 2>/dev/null || true` to ignore error if already exists
podman volume create emqx_data
podman volume create emqx_log
podman volume create emqx_etc
podman run --rm --name broker -p 18083:18083 -p 1883:1883 --init -d \
-e EMQX_TELEMETRY__ENABLE=false \
-v emqx_data:/opt/emqx/data \
-v emqx_etc:/opt/emqx/etc \
-v emqx_log:/opt/emqx/log \
emqx:latest
```

## Authentication

If Authentication is enabled, you can use the `scripts/users.py` to generate users and passwords:

The csv template (<https://www.emqx.io/docs/en/latest/access-control/authn/user_management.html#importing-users>) is:

```csv
user_id,password_hash,salt,is_superuser
user1,pass1,...,false
user2,pass2,...,false
```

```bash
# modify .env or export:
export MQTT_USERS=user1,user2,user3
export MQTT_PASSWORDS=pass1,pass2,pass3
# on windows, use $env:MQTT_USERS and $env:MQTT_PASSWORDS:
# $env:MQTT_USERS="user1,user2,user3"
# $env:MQTT_PASSWORDS="pass1,pass2,pass3"
python scripts/users.py
```

You can then import the `users.csv` file in the dashboard.
![import users](./users.png)

## License

[MIT](./LICENSE)

EMQX is licensed under the Apache License 2.0: <https://github.com/emqx/emqx/blob/master/LICENSE>
