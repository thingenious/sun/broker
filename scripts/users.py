"""Generate users.csv file with initial users and their passwords."""

import csv
import hashlib
import os
import secrets
from typing import List, Tuple

try:
    from dotenv import load_dotenv
except ImportError:
    pass
else:
    load_dotenv()


CSV_HEADER = "user_id,password_hash,salt,is_superuser"


def generate_password_hash(password: str, salt: str) -> str:
    """Generate password hash using sha256 algorithm."""
    # from emqx config:
    # password_hash_algorithm {
    #   name = "sha256",
    #   salt_position = "prefix",
    # }
    return hashlib.sha256(f"{salt}{password}".encode()).hexdigest()


def generate_salt() -> str:
    """Generate random salt."""
    return secrets.token_hex(16)


def get_credentials() -> Tuple[List[str], List[str]]:
    """Get users from environment variable."""
    # export MQTT_USERS=user1,user2,user3
    # export MQTT_PASSWORDS=pass1,pass2,pass3
    users = os.environ.get("MQTT_USERS", "")
    passwords = os.environ.get("MQTT_PASSWORDS", "")
    return users.split(","), passwords.split(",")


def main() -> None:
    """Generate users.csv file with initial users and their passwords."""
    users, passwords = get_credentials()
    if len(users) != len(passwords):
        raise ValueError("Number of users and passwords does not match")
    if not users:
        raise ValueError("No users provided")
    parent_dir = os.path.dirname(os.path.dirname(__file__))
    csv_path = os.path.realpath(os.path.join(parent_dir, "users.csv"))
    with open(csv_path, "w", newline="", encoding="utf-8") as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(CSV_HEADER.split(","))
        for user, password in zip(users, passwords):
            salt = generate_salt()
            password_hash = generate_password_hash(password, salt)
            writer.writerow([user, password_hash, salt, "false"])
    relative = csv_path.replace(os.getcwd(), "")
    if relative.startswith(os.sep):
        relative = relative[len(os.sep) :]
    print(f"Generated: {relative}")


if __name__ == "__main__":
    main()
