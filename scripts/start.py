"""Start an emqx docker container."""

import os
import secrets
import shutil
import subprocess  # nosemgrep # nosec
import sys
from pathlib import Path
from typing import List

try:
    from dotenv import load_dotenv
except ImportError:
    pass
else:
    load_dotenv()


IMAGE_NAME = os.environ.get("BROKER_IMAGE", "docker.io/emqx/emqx:latest")
DEFAULT_CONTAINER_NAME = os.environ.get("BROKER_CONTAINER_NAME", "broker")
PORTS_MAPPING = {18083: 18083, 1883: 1883}
MQTT_ERLANG_COOKIE = os.environ.get("MQTT_ERLANG_COOKIE", "")
if not MQTT_ERLANG_COOKIE:
    MQTT_ERLANG_COOKIE = secrets.token_hex(32)


def run_command(cmd: List[str], allow_error: bool = True) -> None:
    """Run command."""
    cwd = Path(__file__).parent.parent.resolve()
    try:
        subprocess.run(
            cmd,
            check=True,
            cwd=cwd,
            env=os.environ,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE if allow_error else subprocess.DEVNULL,
        )  # nosemgrep # nosec
    except subprocess.CalledProcessError as error:
        if allow_error:
            return
        raise RuntimeError(f"Error running command: {error}") from error


def get_default_container_command() -> str:
    """Get the container command."""
    # prefer podman over docker if found
    from_env = os.environ.get("CONTAINER_COMMAND", "")
    if from_env in ("docker", "podman"):
        return from_env
    if shutil.which("podman"):
        return "podman"
    if not shutil.which("docker"):
        raise RuntimeError("Could not find docker or podman.")
    return "docker"


def get_volume_args(container_command: str) -> List[str]:
    """Create container volumes."""
    volume_names = ["emqx_data", "emqx_log", "emqx_etc"]
    # -v emqx_data:/opt/emqx/data
    # -v emqx_log:/opt/emqx/log
    # -v emqx_etc:/opt/emqx/etc
    volume_args = []
    for volume_name in volume_names:
        run_command([container_command, "volume", "create", volume_name])
        container_path = f"/opt/{volume_name.replace('_', '/')}"
        volume_args.extend(["-v", f"{volume_name}:{container_path}"])
    return volume_args


def get_env_args() -> List[str]:
    """Get environment variable arguments to include."""
    args = [
        "-e",
        "EMQX_TELEMETRY__ENABLE=false",
        "-e",
        "EMQX_NODE_MAX_PORTS=1034",
        "-e",
        "EMQX_NODE_PROCESS_LIMIT=1034",
        "-e",
        f"EMQX_NODE__COOKIE={MQTT_ERLANG_COOKIE}",
    ]
    auth_enabled_str = os.environ.get("MQTT_AUTHENTICATION_ENABLED", "false")
    if not auth_enabled_str:
        auth_enabled_str = "false"
    auth_enabled = auth_enabled_str.lower()[0] in ("t", "y", "1")
    if auth_enabled:
        args.extend(
            [
                "-e",
                "EMQX_AUTHENTICATION__1__ENABLE=true",
                "-e",
                "EMQX_AUTHENTICATION__1__BACKEND=built_in_database",
                "-e",
                "EMQX_AUTHENTICATION__1__MECHANISM=password_based",
                "-e",
                "EMQX_AUTHENTICATION__1__PASSWORD_HASH_ALGORITHM__NAME=sha256",
                "-e",
                "EMQX_AUTHENTICATION__1__PASSWORD_HASH_ALGORITHM__SALT_POSITION=prefix",
                "-e",
                "EMQX_AUTHENTICATION__1__USER_ID_TYPE=username",
            ]
        )
    return args


def get_port_args() -> List[str]:
    """Get ports to map."""
    args = []
    for host_port, container_port in PORTS_MAPPING.items():
        args.extend(["-p", f"{host_port}:{container_port}"])
    return args


def main() -> None:
    """Get the command to call."""
    container_cmd = get_default_container_command()
    container_name = DEFAULT_CONTAINER_NAME
    if "--container-name" in sys.argv and len(sys.argv) > 1:
        name_index = sys.argv.index("--container-name") + 1
        if name_index < len(sys.argv):
            container_name = sys.argv[name_index]
    if "--docker" in sys.argv and shutil.which("docker") is not None:
        container_cmd = "docker"
    if "--podman" in sys.argv and shutil.which("podman") is not None:
        container_cmd = "podman"
    # stop if already running
    run_command([container_cmd, "stop", container_name], allow_error=True)
    #
    command = [container_cmd, "run", "--rm", "--name", container_name, "--init", "-d"]
    env_args = get_env_args()
    command.extend(env_args)
    port_args = get_port_args()
    command.extend(port_args)
    volume_args = get_volume_args(container_cmd)
    command.extend(volume_args)
    command.append(IMAGE_NAME)
    print(f"Calling: {' '.join(command)}")
    run_command(command)
    print(f"Use (with -f to follow): {container_cmd} logs {container_name}")
    print("To view the container's logs.")


if __name__ == "__main__":
    main()
